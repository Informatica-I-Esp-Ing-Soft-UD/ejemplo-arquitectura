from flask import Flask, url_for, redirect
from flask.helpers import url_for
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS, cross_origin

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///domiciliarios.db'
app.config['SECRET_KEY'] = "123"
app.config['CORS_HEADERS'] = 'Content-Type'

cors = CORS(app)
db = SQLAlchemy(app)

class domiciliario(db.Model):
    id = db.Column("domiciliary_id", db.Integer, primary_key = True)
    domiciliario_nombre = db.Column(db.String(100))
    domiciliario_telefono = db.Column(db.Integer)

    def __init__(self, datos):
        self.domiciliario_nombre = datos["nombre"]
        self.domiciliario_telefono = datos["telefono"]

@app.route("/")
@cross_origin()
def principal():
    data = domiciliario.query.all()
    diccionario_clientes = {}
    for r in data:
        d = {
            "id": r.id, 
            "nombre": r.domiciliario_nombre,
            "telefono": r.domiciliario_telefono
        }
        diccionario_clientes[r.id] = d
    return diccionario_clientes

@app.route("/agregar/<nombre>/<int:telefono>")
@cross_origin()
def agregar(nombre, telefono):
    datos = {
        "nombre": nombre,
        "telefono": telefono
    }
    d = domiciliario(datos)
    db.session.add(d)
    db.session.commit()
    return redirect(url_for('principal'))

@app.route("/eliminar/<int:id>")
@cross_origin()
def eliminar(id):
    d = domiciliario.query.filter_by(id=id).first()
    db.session.delete(d)
    db.session.commit()
    return redirect(url_for('principal'))

@app.route("/actualizar/<int:id>/<nombre>/<int:telefono>")
@cross_origin()
def actualizar(id, nombre, telefono):
    d = domiciliario.query.filter_by(id=id).first()
    d.domiciliario_nombre = nombre
    d.domiciliario_telefono = telefono
    db.session.commit()
    return redirect(url_for('principal'))

@app.route("/buscar/<int:id>")
def buscar(id):
    r = domiciliario.query.filter_by(id=id).first()
    d = { 
        "id": r.id, 
        "nombre": r.domiciliario_nombre,
        "telefono": r.domiciliario_telefono
    }
    return d

if __name__== "__main__":
    db.create_all()
    app.run(debug=True)