from flask import Flask, url_for, redirect
from flask.helpers import url_for
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS, cross_origin
from werkzeug.routing import BaseConverter

class IntListConverter(BaseConverter):
    regex = r'\d+(?:,\d+)*,?'

    def to_python(self, value):
        return [int(x) for x in value.split(',')]

    def to_url(self, value):
        return ','.join(str(x) for x in value)

app = Flask(__name__)
app.url_map.converters['int_list'] = IntListConverter
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///pedidos.db'
app.config['SECRET_KEY'] = "123"
app.config['CORS_HEADERS'] = 'Content-Type'

cors = CORS(app)
db = SQLAlchemy(app)

class pedido(db.Model):
    id = db.Column("order_id", db.Integer, primary_key = True)
    pedido_cliente = db.Column(db.Integer)
    pedido_domiciliario = db.Column(db.Integer)
    pedido_items = db.Column(db.String(200))
    pedido_estado = db.Column(db.String(100))

    def __init__(self, datos):
        self.pedido_cliente = datos["cliente"]
        self.pedido_domiciliario = datos["domiciliario"]
        self.pedido_items = ' '.join(str(i) for i in datos["items"])
        self.pedido_estado = datos["estado"]

@app.route("/")
@cross_origin()
def principal():
    data = pedido.query.all()
    diccionario_pedidos = {}
    for d in data:
        p = { 
            "id": d.id, 
            "cliente": d.pedido_cliente,
            "domiciliario": d.pedido_domiciliario,
            "items": [int(i) for i in d.pedido_items.split(' ')],
            "estado": d.pedido_estado
        }
        diccionario_pedidos[d.id] = p
    return diccionario_pedidos

@app.route("/agregar/<int:cliente>/<int:domiciliario>/<int_list:items>/<estado>")
@cross_origin()
def agregar(cliente, domiciliario, items, estado):
    datos = {
        "cliente": cliente,
        "domiciliario": domiciliario,
        "items": items,
        "estado": estado
    }
    p = pedido(datos)
    db.session.add(p)
    db.session.commit()
    return redirect(url_for('principal'))

@app.route("/eliminar/<int:id>")
@cross_origin()
def eliminar(id):
    p = pedido.query.filter_by(id=id).first()
    db.session.delete(p)
    db.session.commit()
    return redirect(url_for('principal'))

@app.route("/actualizar/<int:id>/<int:cliente>/<int:domiciliario>/<int_list:items>/<estado>")
@cross_origin()
def actualizar(id, cliente, domiciliario, items, estado):
    p = pedido.query.filter_by(id=id).first()
    p.pedido_cliente = cliente
    p.pedido_domiciliario = domiciliario
    p.pedido_items = ' '.join(str(i) for i in items)
    p.pedido_estado = estado
    db.session.commit()
    return redirect(url_for('principal'))

@app.route("/buscar/<int:id>")
def buscar(id):
    d = pedido.query.filter_by(id=id).first()
    p = { 
        "id": d.id, 
        "cliente": d.pedido_cliente,
        "domiciliario": d.pedido_domiciliario,
        "items": [int(i) for i in d.pedido_items.split(' ')],
        "estado": d.pedido_estado
    }
    return p

if __name__== "__main__":
    db.create_all()
    app.run(debug=True)