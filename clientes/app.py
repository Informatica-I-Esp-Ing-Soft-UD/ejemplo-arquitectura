from flask import Flask, url_for, redirect
from flask.helpers import url_for
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS, cross_origin

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///clientes.db'
app.config['SECRET_KEY'] = "123"
app.config['CORS_HEADERS'] = 'Content-Type'

cors = CORS(app)
db = SQLAlchemy(app)

class cliente(db.Model):
    id = db.Column("customer_id", db.Integer, primary_key = True)
    cliente_nombre = db.Column(db.String(100))
    cliente_telefono = db.Column(db.Integer)
    cliente_direccion = db.Column(db.String(30))

    def __init__(self, datos):
        self.cliente_nombre = datos["nombre"]
        self.cliente_telefono = datos["telefono"]
        self.cliente_direccion = datos["direccion"]

@app.route("/")
@cross_origin()
def principal():
    data = cliente.query.all()
    diccionario_clientes = {}
    for d in data:
        c = {
            "id": d.id, 
            "nombre": d.cliente_nombre,
            "telefono": d.cliente_telefono,
            "direccion": d.cliente_direccion
        }
        diccionario_clientes[d.id] = c
    return diccionario_clientes

@app.route("/agregar/<nombre>/<int:telefono>/<direccion>")
@cross_origin()
def agregar(nombre, telefono, direccion):
    datos = {
        "nombre": nombre,
        "telefono": telefono,
        "direccion": direccion
    }
    c = cliente(datos)
    db.session.add(c)
    db.session.commit()
    return redirect(url_for('principal'))

@app.route("/eliminar/<int:id>")
@cross_origin()
def eliminar(id):
    c = cliente.query.filter_by(id=id).first()
    db.session.delete(c)
    db.session.commit()
    return redirect(url_for('principal'))

@app.route("/actualizar/<int:id>/<nombre>/<int:telefono>/<direccion>")
@cross_origin()
def actualizar(id, nombre, telefono, direccion):
    c = cliente.query.filter_by(id=id).first()
    c.cliente_nombre = nombre
    c.cliente_telefono = telefono
    c.cliente_direccion = direccion
    db.session.commit()
    return redirect(url_for('principal'))

@app.route("/buscar/<int:id>")
def buscar(id):
    d = cliente.query.filter_by(id=id).first()
    c = { 
        "id": d.id, 
        "nombre": d.cliente_nombre,
        "telefono": d.cliente_telefono,
        "direccion": d.cliente_direccion
    }
    return c

if __name__== "__main__":
    db.create_all()
    app.run(debug=True)